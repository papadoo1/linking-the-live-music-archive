# Enhancing the ETREE dataset
This project's goal is to provide a framework for external alignments to to the Artist and Venue entities of
the [Etree](etree.linkedmusic.com) dataset with Last.fm and MusicBrainz

t - test folder   
lib - perl modules written using the [Moose](http://moose.iinteractive.com/en/) postmodern object system  
bin - includes a simple cli front-end, a statistics printout script, and a run_tests script

