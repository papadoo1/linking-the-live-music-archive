#!/usr/bin/perl -w
use strict;
use lib '../lib';
use RDF::Query::Client;
use Data::Dumper;
use RDF::SparqlQueries;

my $p = $RDF::SparqlQueries::prefix;

print ("Claculating live statistics for mappings...\n");

my $allArtists = $p."SELECT DISTINCT ?a WHERE {?a rdf:type mo:MusicArtist}";
my $artistCount;

# count
my $c = new RDF::Query::Client($allArtists);
my $result = $c -> execute('http://etree.linkedmusic.org/sparql');

while (my $row = $result -> next) {
    $artistCount++
}

printf($artistCount." artists currently in the database.\n");


my $seanMbMappingsq = <<QUERY;
SELECT DISTINCT ?a ?weight WHERE {
  ?a rdf:type mo:MusicArtist.
  ?sim sim:subject ?a.
  ?sim sim:method etree:simpleMusicBrainzMatch.
  ?sim sim:weight ?weight
}
QUERY

my $seanMbMappingsCount;
my %seanMbMappingsArtists;
my $seanMbMappingsClient = new RDF::Query::Client($p.$seanMbMappingsq);
$result = $seanMbMappingsClient -> execute('http://etree.linkedmusic.org/sparql');
while (my $row = $result -> next)
{
    $seanMbMappingsCount++;
    $seanMbMappingsArtists{$row -> {'a'} -> uri_value} = $row -> {'weight'} -> literal_value;
}
#-----------------------------------------
my $orestisMbMappingsq = <<QUERY;
SELECT DISTINCT ?a ?weight WHERE {
  ?a rdf:type mo:MusicArtist.
  ?sim sim:subject ?a.
  ?sim sim:method etree:opMusicBrainzMatch.
  ?sim sim:weight ?weight
}
QUERY
my $orestisMbMappingsCount;
my %orestisMbMappingsArtists;
my $orestisMbMappingsClient = new RDF::Query::Client($p.$orestisMbMappingsq);
$result = $orestisMbMappingsClient -> execute('http://etree.linkedmusic.org/sparql');
while (my $row = $result -> next)
{
    $orestisMbMappingsCount++;
    $orestisMbMappingsArtists{$row -> {'a'} -> uri_value} = $row -> {'weight'} -> literal_value;
}
#----------------------------------------------
my $orestisLFMMappingsq = <<QUERY;
SELECT DISTINCT ?a WHERE {
  ?a rdf:type mo:MusicArtist.
  ?sim sim:subject ?a.
  ?sim sim:method etree:opLastFMMatch
}
QUERY
my $orestisLFMMappingsCount;
my %orestisLFMappingsArtists;
my $orestisLFMMappingsClient = new RDF::Query::Client($p.$orestisLFMMappingsq);
$result = $orestisLFMMappingsClient -> execute('http://etree.linkedmusic.org/sparql');
while (my $row = $result -> next)
{
    $orestisLFMMappingsCount++;
    $orestisLFMappingsArtists{$row -> {'a'} -> uri_value} = 1;
}

printf ("SeanMB: %d mappings\nOrestis MB: %d mappings\nOrestis LASTFM: %d mappings\n", $seanMbMappingsCount, $orestisMbMappingsCount, $orestisLFMMappingsCount);

# Music brainz matches that I got and Sean didn't

my $extra;
my $corrected; #think they are zero
foreach my $uri (keys %orestisMbMappingsArtists)
{
    if (not $seanMbMappingsArtists{$uri}) {
        $extra++;
    }
    else
    {
        if ($orestisMbMappingsArtists{$uri} == 0) { 
           $corrected++;
        }
        
    }
}
printf ("Orestis added %d entirely new MusicBrainz mappings\n", $extra );
printf ("And suggested that %d of Sean's MusicBrainz mappings are wrong\n", $corrected);
