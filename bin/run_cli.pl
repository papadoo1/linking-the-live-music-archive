#!/usr/bin/perl

use Getopt::Long;
use Data::Dumper;
use lib "../lib";
use Switch;
use Matcher::LastFMMatcher;
use Matcher::MusicBrainzMatcher;
use strict;
use feature qw(say);
package run_cli;

my $welcomeMessage = <<END;
|     *Etree Data Linker*     |
|             v1              |
|                             |
|      Author:Orestis         |
|             Papadopoulos    |
|                             |
|      2014                   |   
END

my $menu1 = <<MENU1;

Select the type of data to be aligned:
1. Artist
2. Location
3. Venue
4. ?
MENU1

print $welcomeMessage;

my $exit = 0;
while (not $exit)
{    
    print $menu1;
    print ":";
    my $choice1 = <STDIN>;
    chomp $choice1;
    print "\n";
    switch($choice1){
      case 1        { artistAlignmentMenu(); }
      case 2        { say 'SORRY - NOT IMPLEMENTED'; }
      case 3        { say 'SORRY - NOT IMPLEMENTED'; }
      case 4        { say '?';}
      case 'exit'   { say "exiting..."; $exit = 1;}
      else          { say 'Enter a number between 1 and 4' }
    }   
}

sub artistAlignmentMenu
{
    my $artistMenu = <<MENU;
An etree artist entity will be matched with another artist entity in an external source
Here is a list of available APIs to select:

1.LastFM API
2.MusicBrainz API

(type back to go back)
MENU
    print $artistMenu;
    print ":";
    my $apiChoice = <STDIN>;
    chomp $apiChoice;
    print "\n";
    switch($apiChoice)
    {
        case 1      {lastFMAlignment();}
        case 2      {musicBrainzAlignment();}
        case 'back' {return;}
        case 'exit' {$exit = 1; return;}
        else        {say "1-2 back or exit";}
    }
    return 1;
}

sub musicBrainzAlignment
{
  print "Etree artist name (defaults to ALL):" ;
  my $a_name = <STDIN>;
  chomp $a_name;
  
  print "Limit(defaults to no_limit):";
  my $limit = <STDIN>;
  chomp $limit;
  if (length $limit eq 0) {
    $limit = 0;
  }
  
  print "Verbosity(defaults to 0):";
  my $v = <STDIN>;
  chomp $v;
  
  my $lfmmatcher = new Matcher::MusicBrainzMatcher(verbose => $v);
  if ($lfmmatcher -> setup(searchArtist => $a_name,
                           noOfArtists => $limit
                           )
                          )
  {
    $lfmmatcher -> run(); 
  }
    
}

sub lastFMAlignment()
{
  print "Etree artist name (defaults to ALL):" ;
  my $a_name = <STDIN>;
  chomp $a_name;
  
  print "Limit(defaults to no_limit):";
  my $limit = <STDIN>;
  chomp $limit;
  if (length $limit eq 0) {
    $limit = 0;
  }
  
  print "Verbosity(defaults to 0):";
  my $v = <STDIN>;
  chomp $v;
  
  my $lfmmatcher = new Matcher::LastFMMatcher(verbose => $v);
  if ($lfmmatcher -> setup(searchArtist => $a_name,
                           noOfArtists => $limit
                           )
                          
                          )
  {
    $lfmmatcher -> run(); 
  }
}