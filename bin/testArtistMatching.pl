#!/usr/bin/perl -w
use strict;
use Data::Dumper;
use etree;
use musicBrainzMatch;
use feature qw(say);

# Script that helps in testing the alignments

#my $test = {'name' => "American Minor"};
#musicBrainzMatch::align($test);
my $filename = shift;
my $start = shift;
# TODO make it read from a dump file
# TODO make it delete the results file

my $arrayRef = etree::getAllArtists();
# this only gets the artists that don't have a sim in the database
#my $arrayRef = etree::getAllUnmatchedArtists();

# Loop starting from a point in the list
say "Starting from $start";
for my $i ($start..scalar @{$arrayRef} - 1)
{
    # hash reference
    my $artist = @{$arrayRef}[$i];
    if (not ($artist -> {'name'} eq ''))
    {
        say $i.".".$artist -> {"name" }; 
        musicBrainzMatch::align($artist, $filename);
    }
}

print "Done\n";

