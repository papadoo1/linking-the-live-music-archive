package Helper::Other;

# passing an array argument returns the same array eliminating the duplicates
# preserves order
# converts to lowercase
# strips trailing spaces
sub unique {
    my %seen = ();
    my @r = ();
    foreach my $a (@_) {
        #convert to lowercase
        $a = lc $a;
        # removing trailing and ending whitespace
        $a =~ s/^\s+//;
        $a =~ s/\s+$//;
        unless ($seen{$a}) {
            push @r, $a;
            $seen{$a} = 1;
        }
    }
    return @r;
}

sub arrayMax
{
    my $arr = shift;
    my $max = ${$arr}[0];
    my $imax = 0;
    for (my $i = 1; $i < scalar @{$arr}; $i++)
    {
        if (@{$arr}[$i] > $max) {
            $max = @{$arr}[$i];
            $imax = $i;
        }
    }
    return $imax;
}

sub arrayMin
{
    my $arr = shift;
    my $min = ${$arr}[0];
    my $imin = 0;
    for (my $i = 1; $i < scalar @{$arr}; $i++)
    {
        if (@{$arr}[$i] < $min) {
            $min = @{$arr}[$i];
            $imin = $i;
        }
    }
    return $imin;
}

sub getTimestamp
{
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
  my $nice_timestamp = sprintf ( "%02d%02d%02d", $hour,$min,$sec);
  return $nice_timestamp;
}

1;
