#!/usr/bin/perl -w
package NameComparable;

use Moose::Role;
use Text::LevenshteinXS qw(distance);

requires 'name';

# returns the distance between the 2 compared objects
sub cmp
{
    my $self = shift;
    my $other = shift;
    my $d = distance(lc $self -> name(), lc $other -> name());
    
    #if (length $self -> name > 0)
    #{
    #        return 1-($d/length $self->name)
    #}
    
    return $d;
}

1;