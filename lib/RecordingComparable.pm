#!/usr/bin/perl -w
package RecordingComparable;
use Moose::Role;

requires 'recordings', 'cmp';

sub cmp_recordings
{
    my $self = shift;
    my $other = shift;
    
    my $matched = 0;
    my $count = 0;
    foreach my $selfTrack (keys %{$self->recordings()})
    {
        if ($other -> recordings() -> {$selfTrack})
        {
            $matched ++;
        }
        $count ++;
    }
    if ($count > 0)
    {
        return $matched/$count
    }
    else
    {
        return 0;
    }
}
1;