#!/usr/bin/perl -w
package InverseSimilarity;

use Moose;
use RDF::RDFExport;

has 'similarity' => (is => 'ro', required => 1, isa => 'Similarity');

# I have left init_arg out, so this attribute can also be explicitly be set
# in the constructor. (even though specifying a different kind of helper will break it)

# stub
sub toString
{
    return;
}

sub toFile #input : (RDF::Helper $rdf, Str $filename)
{
    my $self = shift;
    my $sim = $self -> similarity; 
    
    my $rdf = RDF::RDFExport::getRDFHelper();
    
    my $subjectResource = $rdf -> new_resource($sim -> subject() -> URI());
    my $simResource = $rdf -> new_resource($sim -> URI());
    my $objectResource = $rdf -> new_resource($sim -> object() -> URI());
    
    $rdf -> assert_resource($subjectResource, 'sim:subjectOf', $simResource);
    $rdf -> assert_resource($objectResource, 'sim:objectOf', $simResource);
    
    if (not -d "rdf_out")
    {
        mkdir "rdf_out"
            or die "cannot create directory rdf_out: $!";    
    }
    
    # might break if it calls the ARTIST::id method from the superclass
    my $filename = "rdf_out/".$sim->exportFilename()."_inverse";    

    open(my $fh, ">", $filename)
      or die "cannot open > output.rdf: $!";

    print $fh $rdf -> serialize('format' => 'rdfxml');
    print "Written to file: $filename\n";
}

1;
