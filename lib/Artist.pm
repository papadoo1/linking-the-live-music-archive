#!/usr/bin/perl -w
package Artist;
use Moose;

has 'name' => (is => 'rw', isa => 'Str');
has 'URI' => (is => 'rw', isa => 'Str', required => 1);

sub id
{
    my $self = shift;
    return $self -> URI();
}

with 'NameComparable';

no Moose;
__PACKAGE__->meta->make_immutable;

1;
