#!/usr/bin/perl
package Matcher::LastFMMatcher;
use Moose;
use lib '..';
use API::LastFM;
use Similarity::LFMSim;
use Data::Dumper;

sub match # input: (Artist::EtreeArtist) # output (Similarity::MbSim)
{
  my $self = shift;
  my $etreeArtist = shift;

  # As an even greater abstraction I can make this match method take as a parameter
  # an etree Artist and an object that implements ArtistAPI
  # TODO
  # It's better if the matcher doesn't call the static API
  # instead embed this data as a static method in Artist::MbArtist
  if ($self -> verbose > 0)
  {
    print "\nLASTFM:Searching for ".$etreeArtist->name."...\n";
    sleep(2);
  }
  
  my $lfmArtists = API::LastFM::searchArtist($etreeArtist -> name) or return undef;
  
  if ($self -> verbose > 0)
  {
    print "LASTFM:Found ".scalar @{$lfmArtists}." possible matches\n";
    sleep(2);
  }
  
  my @confidenceArray;
  
  return undef if (scalar @{$lfmArtists} eq 0);
  
  foreach(@{$lfmArtists})
  {
    push(@confidenceArray, $etreeArtist->cmp($_)); # it will call track compare
  }
  
  if ($self -> verbose > 0)
  {
    print "LASTFM:Calculating the best possible match...\n";
    sleep(2);
  }
  my $imin = Helper::Other::arrayMin(\@confidenceArray);
  
  if ($self -> verbose > 0)
  {
    print "LASTFM:Best match is ".@{$lfmArtists}[$imin] -> {URI}."\n";
    sleep(2);
  }
  
  my $minDistance = $confidenceArray[$imin];

  return undef if($minDistance > length $etreeArtist -> name);
  
  my $lfmsim = new Similarity::LFMSim('object' => @{$lfmArtists}[$imin],
				      'subject' => $etreeArtist,
				      'weight' => $minDistance);
  
  return $lfmsim;
}

with 'Matcher'; #make this superclass


1;
