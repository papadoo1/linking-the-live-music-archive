#!/usr/bin/perl
package Matcher::MusicBrainzMatcher;
use Moose;
use lib '..';
use Encode;
use feature qw(say);
use Helper::Other;
use API::MusicBrainz;
use Similarity::MbSim;
use API::LastFM;
use API::Etree; 
use Data::Dumper;

sub match # input: (Artist::EtreeArtist) # output (Similarity::MbSim)
{
  my $self = shift;
  my $etreeArtist = shift;

  # As an even greater abstraction I can make this match method take as a parameter
  # an etree Artist and an object that implements ArtistAPI
  # TODO
  # It's better if the matcher doesn't call the static API
  # instead embed this data as a static method in Artist::MbArtist
  if ($self -> verbose > 0)
  {
    print "\nMUSICBRAINZ:Searching for ".$etreeArtist->name."...\n";
    sleep(2);
  }
  
  my $mbArtists = API::MusicBrainz::searchArtist($etreeArtist -> name()) or return undef;
  
  if ($self -> verbose > 0)
  {
    print "MUSICBRAINZ:Found ".scalar @{$mbArtists}." possible matches\n";
    sleep(2);
  }
  
  my @confidenceArray;
  foreach(@{$mbArtists})
  {
    push(@confidenceArray, $etreeArtist->cmp($_)); # it will call track compare
  }
  
  if ($self -> verbose > 0)
  {
    print "MUSICBRAINZ:Calculating the best possible match...\n";
    sleep(2);
  }
  my $imax = Helper::Other::arrayMax(\@confidenceArray);
  
  my $maxConfidence = $confidenceArray[$imax];

  if ($self -> verbose > 0)
  {
    print "MUSICBRAINZ:Best match is ".@{$mbArtists}[$imax] -> {URI}."\n";
    sleep(2);
  }
  
  # very low conf threshold
  if($maxConfidence < 0.1)
  {
    if ($self -> verbose > 0) {
      print "REJECTED: weight was $maxConfidence\n";
    }
    
    return undef;
  }
  
  if ($self -> verbose > 0) {
      print "ACCEPTED: weight was $maxConfidence\n";
  }
  
  my $mbsim = new Similarity::MbSim('object' => @{$mbArtists}[$imax],
				    'subject' => $etreeArtist,
				    'weight' => $maxConfidence);
  
  return $mbsim;
}

with 'Matcher'; #make this superclass


1;
