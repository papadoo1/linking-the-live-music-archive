#!/usr/bin/perl -w
package Matcher;
use Moose::Role;
use Data::Dumper;
use feature qw(say);

requires 'match';

has 'etreeArtists' => (
                        is => 'rw',
                        isa => "ArrayRef[Artist::EtreeArtist]",
			predicate => 'etreeArtists_set'
                      );
has 'fileExport' => (
                        is => 'rw',
                        isa => "Int",
                        default => 1,
                        reader => 'fileExportIsOn'
                    );
has 'verbose' => (
                    is => 'rw',
                    isa => "Int",
                    default => 0
                 );
has 'simInverse' => (
                        is => 'rw',
                        isa => 'Int',
                        default => 1
                    );
sub setup
{
  my $self = shift;
  my (%info) = @_;

  if ($info{searchArtist} && length $info{searchArtist} > 0)
  { 
    if ($self -> verbose > 0)
    {
        print ("Starting alignment for $info{searchArtist}\n");
    }
    my $matchedArtist = API::Etree::getArtistByName($info{searchArtist});
    if (defined $matchedArtist) {
        $self -> etreeArtists ($matchedArtist);
        return 1;
    }
    else
    {
        return 0;
    }
  }
  # remove API::Etree::Dependency
  # move it into Artist::EtreeArtist::getEtreeArtists
  elsif ($info{noOfArtists} && $info{noOfArtists} > 0)
  {
    # gets all artists
    if ($self -> verbose > 0)
    {
        print ("Starting alignment for $info{noOfArtists} etree Artists\n");
    }
    $self -> etreeArtists (API::Etree::getArtists($info{noOfArtists}));
    return 1;
  }
  else
  {
    # gets all artists
    if ($self -> verbose > 0)
    {
        print ("Starting alignment for ALL etree Artists\n");
    }
    $self -> etreeArtists (API::Etree::getArtists());
    return 1;
  }
}

sub run
{
    my $self = shift;
    
    my $matches = 0;
    
    foreach(@{$self -> etreeArtists()})
    {
        my $sim = $self -> match($_); # returns a Similarity subclass
        if(defined $sim) #if (defined $sim && $self -> verbose());
        {
            $matches ++;
            $sim -> toString() if($self -> verbose() > 1);
            
            if ($self -> fileExportIsOn())
            {
                $sim -> toFile();
                if ($self -> simInverse())
                {
                    $sim -> getInverse() -> toFile()
                }
            }
        }
        else
        {
            say $_->name." not matched";
        }
    }
    my $total = scalar @{$self->etreeArtists()};
    print "Matched: $matches out of ".scalar @{$self->etreeArtists()}."(".(($matches/$total)*100)."%)\n";
}


1;


