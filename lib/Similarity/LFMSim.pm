#!/usr/bin/perl -w
package Similarity::LFMSim;

use lib "..";
use Moose;
use MooseX::ClassAttribute;

extends 'Similarity';

# workaround for class variables
class_has 'wasAttributedTo' => (is => 'ro', default => 'http://etree.linkedmusic.org/person/orestis-papadopoulos');


has '+method' => (default => 'opLastFMMatch');
has '+URI' => (default => sub
                        {
                            my $self = shift;
                            return $self -> subject() -> URI()."/lfm-sim-orestis";
                        });
has '+object' => (isa => 'Artist::LFMArtist');
# We need to explicitly define the mb link as #_ to say that we refer to the entity and
# not the URL
has '+subject' => (isa => 'Artist::EtreeArtist');

has '+exportFilename' => (default => sub{
                                        my $self = shift;
                                        return $self -> subject() -> id()."lfm";
                                        });


override 'updateRdfRepresentation' => sub
{
  my $self = shift;
  my $rdf = $self -> rdfRepresentation();
  my $resource = $rdf -> new_resource ($self -> URI());

  # I might have as well hardcoded them.
  $rdf -> assert_resource($resource, 'prov:wasAttributedTo', Similarity::MbSim::wasAttributedTo());
  $rdf -> assert_resource($resource, 'sim:method', 'etree:'.$self -> method());

  return super();
};

1;