#!/usr/bin/perl -w
package Similarity;

use Moose;
use RDF::RDFExport;
use InverseSimilarity;

has 'object' => (is => 'rw', required => 1, isa => 'Artist');

has 'subject' => (is => 'rw', required => 1, isa => 'Artist');

has 'weight' => (is => 'rw', required => 1, isa => 'Num');

# not really used 
has 'method' => (is => 'ro', required => 1, lazy => 1, default => 'sim');

has 'URI' => (is => 'ro', lazy => 1,
              default => sub
                        {
                            my $self = shift;
                            return $self -> subject() -> URI()."/sim";
                        });
has 'exportFilename' => (is => 'rw' , isa => 'Str', lazy => 1,
                         default => sub{
                                        my $self = shift;
                                        return $self -> subject() -> id();
                                       });

# I have left init_arg out, so this attribute can also be explicitly be set
# in the constructor. (even though specifying a different kind of helper will break it)
has 'rdfRepresentation' => (
                            is => 'ro',
                            isa => 'RDF::Helper',
                            default => sub { RDF::RDFExport::getRDFHelper }
                           );
                         
sub updateRdfRepresentation # input ($resource)
{
    my $self = shift;
    
    my $rdf = $self -> rdfRepresentation();
    
    my $resource = $rdf -> new_resource($self -> URI());
    my $weight = $rdf -> new_literal($self->weight(), undef, "http://www.w3.org/2001/XMLSchema#double");
    
    $rdf -> assert_resource($resource, 'sim:object', $self -> object() -> URI());
    $rdf -> assert_resource($resource, 'sim:subject', $self -> subject() -> URI());
    $rdf -> assert_resource($resource, 'sim:weight', $weight);
    $rdf -> assert_resource($resource, 'rdf:type', 'sim:Similarity');
    
    return 1;
}

sub toString
{
    my $self = shift;
    printf("object: %s (%s)\nsubject: %s(%s)\nweight: %s\n",
           $self -> object() -> name(), $self -> object() -> URI(),
           $self -> subject() -> name(), $self -> subject() -> URI(),                         
           $self -> weight());
}

sub toFile #input : (RDF::Helper $rdf, Str $filename)
{
    my $self = shift;
    
    if (not -d "rdf_out")
    {
        mkdir "rdf_out"
            or die "cannot create directory rdf_out: $!";    
    }
    
        
    my $filename = "rdf_out/".$self -> exportFilename();
    
    $self -> updateRdfRepresentation();
    
    open(my $fh, ">", $filename)
      or die "cannot open > $filename: $!";

    print $fh $self -> rdfRepresentation() -> serialize('format' => 'rdfxml');
    print "Written to file: $filename\n";
}

sub getInverse
{
    my $self = shift;
    return new InverseSimilarity(similarity => $self); 
}

1;
