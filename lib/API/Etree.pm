package API::Etree;

use lib '..';
use Moose;
use Artist::EtreeArtist;
use Data::Dumper;
# not really sure about this yet
use RDF::SparqlQueries;
use RDF::Query::Client;

# Get all the tracks from artist performances
# passing the name, returns a hash
# TODO make this work with a URI instead of a name;
sub getRecordings #in: ($URI) out: hashRef[Int];
{
    my $URI = shift;
    my $query = new RDF::Query::Client(RDF::SparqlQueries::q_getAllPerformances($URI));
    my $result = $query -> execute('http://etree.linkedmusic.org/sparql');
    my %hash;
    # Scan the result iterator to get all the songs
    while(my $row = $result->next)
    {
       my $track = $row -> {'Track'};
       # the following regular expressions remove the " at the end and
       
       # the beggining
       $track =~ s/[^\w\n ]//g;
       
       # we store the tracks in hash and every time a new track is met
       # we increment the counter, this will hopefully give us the most
       # performed tracks i.e the most famous tracks
       $hash{lc $track}++;
    }
    # I am leaving this out for now, I can use it later to short in the init method of etree Artist
    # my @return = sort { $hash{$b} <=> $hash{$a} } keys %hash;
    return \%hash;
}

# returns a hash 
sub getAllSeanMatches
{
    my $query = new RDF::Query::Client($RDF::SparqlQueries::q_selectAllMbMatches);
    my $result = $query -> execute('http://etree.linkedmusic.org/sparql');

    my @results;
    # Scan the result iterator
    while(my $row = $result->next)
    {
	   my $name = $row -> {'artist'};
           my $mbid = $row -> {'mbid'};
	   # the following regular expressions remove the " at the end and 
	   # beggining of the artist names, pain in the ass
	   $name =~ s/^"//;
	   $name =~ s/"$//;
           $mbid =~ s/^"//;
	   $mbid =~ s/"$//;
           
           my $mbmatch = {'artist' => $name,
                         'mbid' => $mbid -> uri_value};
           push(@results, $mbmatch);
    }
    return \@results;
}

sub getArtistByName
{
    my $name = shift;
    my $query = new RDF::Query::Client(RDF::SparqlQueries::q_selectArtistByName($name));
    
    my $result = $query -> execute('http://etree.linkedmusic.org/sparql');
    
    if (not defined ($result)) {die "Something went wrong with the query"}
    
    while(my $row = $result->next)
    {
	   my @results;
           my $URI = $row -> {'URI'} -> uri_value;
	   # the following regular expressions remove the " at the end and 
	   # beggining of the artist names, pain in the ass
	   $name =~ s/^"//;
	   $name =~ s/"$//;
           $URI =~ s/^"//;
	   $URI =~ s/"$//;      
           my $artist = new Artist::EtreeArtist('name' => $name,
						'URI' => $URI);
           push(@results, $artist);
	   return \@results;
    }
    print "Etree: $name couldn't be found in the database\n";
    return undef;
}

sub getArtists
{
    my $limit = shift;
    
    my $query = new RDF::Query::Client(RDF::SparqlQueries::q_selectAllArtists($limit));
    
    my $result = $query -> execute('http://etree.linkedmusic.org/sparql');
    
    if (not defined ($result)) {die "Something went wrong with the query"}
    
    my @linkedTreeArtists;
    # Scan the result iterator
    while(my $row = $result->next)
    {
           my $URI = $row -> {'URI'} -> uri_value;
	   my $name = $row -> {'Artist'};
	   # the following regular expressions remove the " at the end and 
	   # beggining of the artist names, pain in the ass
	   $name =~ s/^"//;
	   $name =~ s/"$//;
           $URI =~ s/^"//;
	   $URI =~ s/"$//;      
           my $artist = new Artist::EtreeArtist('name' => $name,
						'URI' => $URI);
           push(@linkedTreeArtists, $artist);
    }
    return \@linkedTreeArtists;
}


# unmatched is not a good name
# returns all the artists that have no external similarity
sub getAllUnmatchedArtists
{
    # Move this to sparql.pm ?
    my $query = new RDF::Query::Client($RDF::SparqlQueries::q_selectAllUnmatchedArtists);
    my $result = $query -> execute('http://etree.linkedmusic.org/sparql');
    
    if (not defined ($result)) {die "Something went wrong with the query"}
    
    my @linkedTreeArtists;
    # Scan the result iterator
    while(my $row = $result->next)
    {
	   my $name = $row -> {'Artist'};
           my $URI = $row -> {'URI'} -> uri_value;
	   # the following regular expressions remove the " at the end and 
	   # beggining of the artist names, pain in the ass
	   $name =~ s/^"//;
	   $name =~ s/"$//;
           $URI =~ s/^"//;
	   $URI =~ s/"$//;
           
           my $artist = {'name' => $name,
                         'URI' => $URI};
           push(@linkedTreeArtists, $artist);
    }
    return \@linkedTreeArtists;
}
1;
