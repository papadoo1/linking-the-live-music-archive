#/usr/bin/perl -w
package API::Helper;

use HTTP::Request;
use HTTP::Response;
use LWP::UserAgent;

use Exporter;
@ISA = ('Exporter');
@EXPORT_OK=('sendHttpRequest');

my $sleepTime = 5;

sub sendHttpRequest
{
    my $TO_GET = shift;
    my $ua = shift;
    
    my $request = HTTP::Request ->new(GET=> $TO_GET);
    my $useragent = LWP::UserAgent -> new (agent => $ua);
    $useragent -> timeout(5);

    my $response = $useragent -> request($request) or die "Network problem?";    

    while(not ($response -> code eq 200)) {
	# (probably) Bad encoding in the request.
	if ($response -> code eq 400)
	{
	    print "[badencoding]400 for $TO_GET\n";
	    return 0;
	}
	# I overdid it with the requests
	elsif ($response -> code eq 503)
	{
	    print "[blocked] code was ".$response -> code."\n";
	    sleep($sleepTime);
	    $response = $useragent -> request($request);      
	}
	else
	{   # trap
	    print "Code was ".$response -> code."\n";
	    sleep($sleepTime);
	}
    }
    return $response;
}
1;
