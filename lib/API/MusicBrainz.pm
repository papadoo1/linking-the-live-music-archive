#!/usr/bin/perl
package API::MusicBrainz;

use lib "..";
use URI::Escape;
use Data::Dumper;
use XML::Simple;
use API::Helper ('sendHttpRequest');
use Time::HiRes qw(usleep);
use Artist::MbArtist;
use strict;
use warnings;

# the user agent should be meaningful
my $ua = 'Linking the music world/0.2 ( orjpap@gmail.com )';
my $usleepTime = 1000;
# Returns a list of matched artist music brainz ids and their names for the query
# The query is formed by a string passed as an argument 

sub searchArtist # in:($name) out:arrayRef[MbArtist]
{
    my $limit = 5;   
    my $param = uri_escape_utf8 shift;
    
    # Request and strip the XML (maybe move these out to a sub?)
    my $response = sendHttpRequest "http://www.musicbrainz.org/ws/2/artist/?query=artist:\"$param\"&limit=$limit", $ua or return 0;
    my $xmlFile = XMLin($response -> content, KeyAttr => '');
    
    my $resultCount = $xmlFile->{'artist-list'}->{'count'};
    my @resultArray;
    
    if ($resultCount eq 0) {
	return 0;
    }
    elsif ($resultCount == 1)
    {
    	push(@resultArray, $xmlFile -> {'artist-list'} -> {'artist'});
    }
    elsif ($resultCount > 1)
    {
    	push(@resultArray, @{$xmlFile-> {'artist-list'} -> {'artist'}})
    }
     
    # Put the ids and the names in the return array
    my @returnArray = ();
    foreach my $artist (@resultArray)
    {
    	my $mbartist = new Artist::MbArtist("name" => $artist -> {'name'}
				  ,"URI" => "http://musicbrainz.org/artist/".$artist -> {'id'}."#_"
				 );
    	push (@returnArray, $mbartist);
    }
    return \@returnArray;
}


sub getRecordings #in:($mbid) out:hashRef[Int]
{
    my $mbid = shift;
    
    # browse responses for recordings return up to 100 entities
    # with an offset providing paging of these results
    # offset = 1 means from entity 1 and after.
    my $offset = 0;
    my $limit = 100;
    my $overallResultCount = 1;
    my @tracks;
    my %hash;
    
    while($offset <= $overallResultCount)
    {
      my $response = sendHttpRequest "http://www.musicbrainz.org/ws/2/recording?artist=$mbid"
		    ."&limit=$limit&offset=$offset" or return 0; #return a message here ?
      usleep($usleepTime);
      # getting a response (put an eval here so any corrupted XML doesn't crash the program)
      my $xmlFile = XMLin($response -> content, KeyAttr => '');

      $overallResultCount = $xmlFile->{'recording-list'}->{'count'};
      
      # an array holding all the recording entities taken from the xml response
      my @recordingsArray;
      
      # any problematic cases
      if ($overallResultCount eq 0) {
	return \%hash;
      }
      if (ref $xmlFile-> {'recording-list'} -> {'recording'} eq 'ARRAY')
      {
	@recordingsArray = @{$xmlFile-> {'recording-list'} -> {'recording'}};
	foreach my $recording (@recordingsArray)
	{
	    my $recordingTitle = $recording -> {'title'};
	    $recordingTitle =~ s/[^\w ]//g;
	    $hash{lc $recordingTitle}++;
	}    
      }
      else
      {
	my $recordingTitle = $xmlFile-> {'recording-list'} -> {'recording'} -> {'title'};
	$recordingTitle =~ s/[^\w ]//g;
	$hash{lc $recordingTitle} = 1;   
      }
      
      $offset = $offset + 100;
    }#while
    return \%hash;
}
1;


