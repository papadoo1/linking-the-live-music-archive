#!/usr/bin/perl
package API::LastFM;
use lib '..';
use URI::Escape;
use XML::Simple;
use Data::Dumper;
use Time::HiRes qw(usleep);
use Artist::LFMArtist;
use API::Helper qw(sendHttpRequest);

my $API_ROOT = "http://ws.audioscrobbler.com/2.0";
my $artistSearchMethod = "/?method=artist.search";
my $trackSearchMethod = "/?method=track.search";
my $venueSearchMethod = "/?method=venue.search";
my $API_KEY = "api_key=4b2fdcd215d7955fb2fdd74c44bfbf6f";

# stub
sub getArtistTracks
{return undef;}

sub searchArtist # in : $name out : $ ArrayRef[Artist::LFMArtist] might differentiate between by_name methods etc.
{
  # print "\nQuerying last fm for: $_\n";
  my $artistName = uri_escape_utf8 shift;
  
  # make a new HTTP request
  my $response = sendHttpRequest "$API_ROOT$artistSearchMethod&artist=\"$artistName\"&$API_KEY", "linking the music world" or return 0;
    
  # Prevent from dying with eval in case of corrupted XML;
  # TODO better add it to other places as well
  my $XMLFile = eval {XMLin($response -> content, KeyAttr => {lfm => 'artist'})};
  if($@)
  {
    print $@;
    return 0;
  }
  
  my $resultsCount = $XMLFile -> {results} -> {'opensearch:totalResults'};
  my @resultsArray;
  
  if($XMLFile->{status} eq "ok")
  {   
    
    # if it's more than one results then we get back an array of artists
    if($resultsCount > 1) 
    {
      foreach(@{$XMLFile->{results}{artistmatches}{artist}})
      {
        my $URI = $_->{url};
        my $name = $_->{name};
        my $artist = new Artist::LFMArtist (
                                              URI => $URI,
                                              name => $name
                                           );
        push(@resultsArray, $artist);
      }
    }
    # which gives an error when you get only 1 result
    elsif($resultsCount == 1)
    {
      my $artist = new Artist::LFMArtist (
                                            name => $XMLFile->{results}{artistmatches}{artist}{name},
                                            URI => $XMLFile->{results}{artistmatches}{artist}{url}
                                         );
      push(@resultsArray, $artist);
    }
  }
  else
  {
    print $request -> uri."\n";
    # Something went wrong
    print Dumper $XMLFile;
  }
  return \@resultsArray;
}

sub searchVenue
{
  my $venueName = "venue=$_[0]";
  # make a new HTTP request
  my $response = sendHttpRequest "$API_ROOT$venueSearchMethod&$venueName&$API_KEY", "Linking the music world/0.1" or return 0;

  # I don't know why this works, but it prevents the stupid XMLin from changing the order of results
  my $XMLFile = XMLin($response -> content, KeyAttr => {lfm => 'artist'});
  
  # make a Venue object and return it instead of a piece of shit :-)
  return $XMLFile -> {results} -> {venuematches} -> {venue}[0] -> {name};
}


# Gets an array of names and searches for each one if a track matches a track in Last.Fm and
# returns the number of matches. ??? DO NOT USE
sub getTrackSimilarity
{
    my $artistName = "artist=$_[1]";
    my $linkedTracks = $_[0];
    my $trackMatches = 0;

    foreach my $linkedTreeTrack (@$linkedTracks)
    {
      my $request = HTTP::Request -> 
                  new (GET => "$API_ROOT$trackSearchMethod&track=\"$linkedTreeTrack\""
                       ."&$artistName&$API_KEY");

      # Use a meaningful user agent in order not to get banned by last.fm
      my $useragent = LWP::UserAgent -> new (agent => "Linking the music world/0.1");
      $useragent ->timeout(5);
  
      # Get the response and do XML parsing
      my $response = $useragent -> request($request);

      my $XMLFile = XMLin($response -> content, KeyAttr => '');

      # Check how many results were returned
      # We assume if at least one result is returned there is a similar track
      if($XMLFile-> {'results'} -> {'opensearch:totalResults'} > 0)
      {
        $trackMatches ++;
      }
    }
    return $trackMatches;
}


