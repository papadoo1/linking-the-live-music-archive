#!/usr/bin/perl -w
use strict;

# GeoSim represents a match between a venue.location
# and a valid geonames location represented as a GeoLocation object
# also supports a confidence ratio TODO not yet implemented

package GeoSim
{
    use Moose;
    use Venue;
    use GeoLocation;
    use GeoNamesAPI;
    
    # Constructor Declarations
    has 'venue', is => 'ro', isa => 'Venue';
    has 'geolocation', is => 'ro', isa => 'GeoLocation';
    has 'confidence', is => 'ro', isa => 'Int';

    sub toString
    {
        my $self = shift;
        return "Venue: ".$self -> venue() -> location().", "
              ."GeoLocation: ".$self -> geolocation () -> name()."\n";
    }
    
    # performs a geo-location search using the GeoNames API based on location
    # Returns a GeoSim object
    sub find
    {
        my $venue = shift;
        # search will return a geoLocation or null(?) if there is no match
        my $geoLocation = GeoNamesAPI::search($venue -> location());
        
        return GeoSim -> new (
                              "venue" => $venue,
                              "geolocation" => $geoLocation,
                              # confidence under development
                              "confidence" => 0
                             )
    }
}

1;