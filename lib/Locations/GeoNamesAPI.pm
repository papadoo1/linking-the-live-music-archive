#!/usr/bin/perl -w
package GeoNamesAPI
{
    use strict;
    use URI::Escape;
    use GeoLocation;
    use Data::Dumper;
    use HTTP::Request;
    use HTTP::Response;
    use LWP::UserAgent;
    use XML::Simple;
    # User agent
    my $ua = "orjpap";
    # do a string search at Geonames and return a GeoLocation object
    sub simpleQuery
    {
        my $q = uri_escape_utf8(shift);
        my $xmlResponse = xmlRequest("http://api.geonames.org/search?q=$q&username=orjpap");
        return $xmlResponse -> {geoname} [0] -> {name};
    }
    
    # returns an xml response
    sub xmlRequest
    {
        my $URL = shift;
        my $request = HTTP::Request -> new(GET=>$URL);
        my $useragent = LWP::UserAgent -> new (agent => $ua);
        $useragent -> timeout(5);
        # TODO add response code handling here
        my $response = $useragent -> request($request) or die "Network problem?";   
    
    
        my $xmlFile = XMLin($response -> content, KeyAttr => '');
        return $xmlFile;
    }
}
1;

