#!/usr/bin/perl -w
use strict;
package Venue
{
    use Moose;
    has 'name', is => 'ro', isa => 'Str';
    has 'location', is => 'ro', isa => 'Str';
    has 'uri', is => 'ro', isa => 'Str';
}
1;

