#!/usr/bin/perl
package rdfExport;
use RDF::Helper;
use strict;
use other;
use Data::Dumper;

sub similarity
{
  no warnings;

  my $method = shift;
  my $subject = shift;
  my $object = shift;
  my $c = shift;

  my $rdf = RDF::Helper->new(
    BaseInterface => 'RDF::Trine',
    namespaces => {
        'sim' => 'http://purl.org/ontology/similarity/',
        'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'etree' => 'http://etree.linkedmusic.org/vocab/',
        'prov' => 'http://www.w3.org/ns/prov#',
        '#default' => "http://purl.org/rss/1.0/",
   }
   );

  # there should be an if else statement here ? (or back in musicbrainzmatch)
  # so not every stuff is mb-sim
  my $res = $rdf -> new_resource("$subject/mb-sim");
  $rdf -> assert_resource($res, 'sim:method', 'etree:'.$method);
  $rdf -> assert_resource($res, 'sim:object',  $object);
  $rdf -> assert_resource($res, 'sim:subject', $subject);
  $rdf -> assert_resource($res, 'rdf:type', 'sim:Similarity');
  $rdf -> assert_resource($res, 'prov:wasAttributedTo', 'http://etree.linkedmusic.org/person/orestis-papadopoulos');
  $rdf -> assert_literal($res, 'sim:weight', $c );

  my @unique = split(/\//, $subject);
  
  my $filename = "data/rdf/".$unique[scalar(@unique) - 1];
  
  open(my $fh, ">", $filename)
      or die "cannot open > output.rdf: $!";

  print "Output : $filename \n\n";
  print $fh $rdf -> serialize('format' => 'rdfxml');

  close($fh);
}

1;
