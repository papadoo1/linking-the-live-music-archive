#!/usr/bin/perl -w
use strict;
package GeoLocation
{
    use Moose;
    has 'name', is => 'ro', isa => 'Str';
    has 'uri', is => 'ro', isa => 'Str';    
}
1;



