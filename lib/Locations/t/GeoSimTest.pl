#!/usr/bin/perl -w
# Trying to use TDD
use lib '..';
use strict;
use Test::More;
use Venue;
use GeoSim;


# 1
my $venue = Venue -> new (
                          "name" => "Potato Factory",
                          "location" => "Potatoland",
                          "uri" => "not specified yet"
                          );


is(GeoSim::find($venue) -> toString(), "Venue: Potatoland, GeoLocation: This is not a good location\n");

# 2 TODO Hardcode a good location here and return a valid match
$
is(GeoSim::find($venue) -> toString(), "Venue: Oregon, Portland, GeoLocation: PORTLAND, OREGON");

done_testing();





