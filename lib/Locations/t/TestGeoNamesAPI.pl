#!/usr/bin/perl -w
# Trying to use TDD
use lib '..';
use strict;
use Test::More;
use Venue;
use GeoNamesAPI;

is(GeoNamesAPI::simpleQuery("Arta, Greece"), "Arta");
# nonesense query with no returns
is(GeoNamesAPI::simpleQuery("9i45tg4iu5tgi5fijerg"), undef);
done_testing();







