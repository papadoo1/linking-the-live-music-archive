package RDF::SparqlQueries;

use lib '../';
use strict;

# this makes the variables explicit
our ($prefix, $q_smallerQuery, $q_selectAllArtists, $q_selectAllUnmatchedArtists, $q_selectAllMbMatches);
# This file includes prefix variables and all linkedtree sparql
# queries used in the program.

# PREFIXES
$prefix = <<END;
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sim: <http://purl.org/ontology/similarity/>
PREFIX mo: <http://purl.org/ontology/mo/>
PREFIX ov: <http://open.vocab.org/terms/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX etree: <http://etree.linkedmusic.org/vocab/>
PREFIX mb: <http://musicbrainz.org/>
PREFIX geo: <http://www.geonames.org/ontology>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX timeline: <http://purl.org/NET/c4dm/timeline.owl#>
PREFIX event: <http://purl.org/NET/c4dm/event.owl#>
PREFIX time: <http://www.w3.org/2006/time#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
END

# QUERIES
# Selects all the artists that have no external similaritys
$q_selectAllUnmatchedArtists
   =
      "$prefix\n"
   .  "SELECT DISTINCT ?URI ?Artist WHERE {\n"
   .  "?URI rdf:type mo:MusicArtist.\n" 
   .  "?URI foaf:name ?Artist.\n"
   .  "NOT EXISTS { ?URI sim:subjectOf ?sim .}\n}"  
   .  "ORDER BY (?name)";

# query, selects all artists from the database in alphabetical order
$q_selectAllMbMatches
   =
     "$prefix\n"
   . "SELECT ?artist ?mbid\n"
   . "WHERE {?URI rdf:type mo:MusicArtist.\n"
   . "       ?URI foaf:name ?artist.\n"
   . "       ?URI sim:subjectOf ?SIMURI.\n"
   . "       ?SIMURI sim:object ?mbid}\n"
   . "ORDER BY ?artist\n";
sub q_selectAllArtists
{
   
   my $r =  "$prefix\n"
   . "SELECT ?URI ?Artist\n"
   . "WHERE {?URI rdf:type mo:MusicArtist.\n"
   . "       ?URI foaf:name ?Artist}\n"
   . "ORDER BY ?Artist\n";
   
   my $limit = shift;
   $r = $r."LIMIT $limit" if ($limit);
   return $r;
}

sub q_selectArtistByName
{
   my $name = shift;
   my $r =  "$prefix\n"
   . "SELECT ?URI ?Artist\n"
   . "WHERE {?URI rdf:type mo:MusicArtist.\n"
   . "       ?URI foaf:name '$name'}\n";
   return $r;
}
    
   
# small test query
$q_smallerQuery
   = "SELECT DISTINCT *
      WHERE {?s ?p ?o
                     }
      LIMIT 10";

sub q_getAllLocations
{
   # fix this return
   return   "$prefix\n"
   .  "SELECT DISTINCT ?URI ?Artist WHERE {\n"
   .  "?URI rdf:type mo:MusicArtist.\n" 
   .  "?URI foaf:name ?Artist.\n"
   .  "NOT EXISTS { ?URI sim:subjectOf ?sim .}\n}"  
   .  "ORDER BY (?name)";
}
sub q_getAllPerformances #in: ($URI)
{
   return "$prefix\n"
         ."SELECT ?Track WHERE{\n"
         ."<$_[0]> mo:performed ?p.\n"
         ."?p event:hasSubEvent ?s.\n"
         ."?s skos:prefLabel ?Track"
         ."}";
}

