#!/usr/bin/perl
package RDF::RDFExport; #need to change this name
# stupid shit
use RDF::Helper;
#use RDF::Trine::Statement;
#use RDF::Trine::Store::Memory;
#use Class::RDF;
use strict;
use Data::Dumper;

sub getRDFHelper
{
  local $SIG{__WARN__} = sub { };
  
  return RDF::Helper->new(
    BaseInterface => 'RDF::Trine',
    namespaces => {
        'sim' => 'http://purl.org/ontology/similarity/',
        'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'etree' => 'http://etree.linkedmusic.org/vocab/',
        'prov' => 'http://www.w3.org/ns/prov#',
        '#default' => "http://purl.org/rss/1.0/",
   },
   ExpandQNames => 1
   );
}

1;
