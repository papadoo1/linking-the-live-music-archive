#!/usr/bin/perl -w
package Artist::EtreeArtist;
use Moose;
use lib '..';
use API::Etree;

extends 'Artist';

has 'recordings' => (is => 'ro',
                  isa => 'HashRef[Int]',
                  lazy => 1, #default will not be generated until it's needed
                  builder => '_init_recordings');

# get the id part of the URI

override 'id' => sub
{
    my $self  = shift;
    my @unique = split(/\//, $self -> URI());
    return $unique[scalar(@unique) - 1];
};

with 'RecordingComparable';

sub _init_recordings
{
    my $self = shift;
    return API::Etree::getRecordings($self -> URI());
}

sub cmp
{
    my $self = shift;
    my $other = shift;
    
    if ($other->DOES('RecordingComparable')) {
        return cmp_recordings($self, $other);
    }
    else
    {
        # hmmmm ...
        return $self -> SUPER::cmp($other);
    }
}

__PACKAGE__->meta->make_immutable;

1;




