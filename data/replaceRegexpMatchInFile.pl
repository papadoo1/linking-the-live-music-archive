#!/usr/bin/perl -w

use IO::File;
use feature qw(say);

opendir (DIR, ".") or die $!;

while (my $file = readdir(DIR)) 
{
      open FILE, $file or die $!;
      my $writefile = 0;
      my @lines;
      while (my $line = <FILE>) 
      {
  	if ($line =~ m/musicbrainz.org\/artist\/(.*)"\/>/g)
        {
           # my $fixed = $1."^^<http://www.w3.org/2001/XMLSchema#double>";
           #$line = "<sim:weight rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">$1</sim:weight>";
           $writefile = 1;
           my $hehe = $1."#_";
           $line =~ s/$1/$hehe/g;
        }
        push(@lines, $line);
      }
      if($writefile)
      {
  	open (MYFILE, '>', "mod/$file") or die $!;
      	foreach(@lines)
      	{
       	  print MYFILE $_;
      	}
      	close MYFILE;
      }
      close FILE;
}



