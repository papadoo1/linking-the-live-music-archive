#!/usr/bin/perl -w
#Dumps all sean's matches into a file
use strict;
use etree;
use Data::Dumper;

my @matches = @{etree::getAllSeanMatches()};
my $filename = shift;

open(my $fh, '>>', $filename) or die "Could not open file '$filename' $!";

# write the musicbrainz id to file
foreach(@matches)
{
   print $fh $_->{mbid}."\t".$_->{artist}."\n";
}
close $fh;    
print "Done\n";

