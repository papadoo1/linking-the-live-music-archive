#!/usr/bin/perl -w
# MusicBrainz.pl --- 
# Author: Orestis Papadopoulo <papadoo1@papadoo-Dell-Netbook>
# Created: 17 Mar 2014
# Version: 0.01

use warnings;
use strict;

use lib '../lib';
use strict;
use Data::Dumper;
use Test::More;
use API::MusicBrainz;

my $artists = API::MusicBrainz::searchArtist("Eminem");
ok (scalar @{$artists} gt 0, 'MB querying returns something!');
is (@{$artists}[0] -> isa('Artist::MbArtist'), 1, 'searchArtist method returns MBArtist references!');

$artists = API::MusicBrainz::searchArtist("");
ok ($artists eq 0, 'Empty query is handled correctly');

$artists = API::MusicBrainz::searchArtist("&");
is (scalar @{$artists}, 5, 'Special character query is handled correctly');

# Music brainz API code returns 0 for rubbish, not an empty array ref TODO change for consistency
$artists = API::MusicBrainz::searchArtist("paaaaaaapapapapappapaparD");
ok ($artists eq 0, 'no results are returned for rubbish!');
done_testing();

__END__

=head1 NAME

MusicBrainz.pl - Describe the usage of script briefly

=head1 SYNOPSIS

MusicBrainz.pl [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation for MusicBrainz.pl, 

=head1 AUTHOR

Orestis Papadopoulos, E<lt>papadoo1papadoo-Dell-NetbookE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2014 by Orestis Papadopoulos

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut
