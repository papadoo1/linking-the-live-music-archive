#!/usr/bin/perl -w
# Trying to use TDD
use lib '../lib';
use strict;
use Data::Dumper;
use Test::More;
use API::LastFM;

my $artists = API::LastFM::searchArtist("Eminem");
ok (scalar @{$artists} gt 0, 'LastFM querying returns something!');
is (@{$artists}[0] -> isa('Artist::LFMArtist'), 1, 'searchArtist method returns LFMArtist references!');

$artists = API::LastFM::searchArtist("");
ok (scalar @{$artists} eq 0, 'Empty query is handled correctly');

$artists = API::LastFM::searchArtist("&");
is (scalar @{$artists}, 30, 'Special character query is handled correctly');

$artists = API::LastFM::searchArtist("paaaaaaapapapapappapaparD");
ok (scalar @{$artists} eq 0, 'no results are returned for rubbish!');
done_testing();


