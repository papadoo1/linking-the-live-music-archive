#!/usr/bin/perl -w
# Trying to use TDD 
use lib '../lib';
use strict;
use feature qw(say);
use Data::Dumper;
use Test::More;
use API::Etree;
use Artist::EtreeArtist;
use Artist::LFMArtist;

my $opt = shift;

my $mastodonQuery = API::LastFM::searchArtist("Mastodon");
my $artist = @{$mastodonQuery}[0];
my $otherArtist = Artist::EtreeArtist -> new ( name => 'Mastodon', URI => 'fake.com' );

# Initialisation tests
# check if the object is initialised correctly
is($artist->name(), 'Mastodon', "Name initialisation works");
is($artist->URI(), 'http://www.last.fm/music/Mastodon', "URI initialisation works");
# id in artist is inherited by the Artist base class which just returns the URI
is($artist->id(), $artist -> URI(), "ID initialisation works and id is meaningful (=URI)");

# Comparison tests
is($otherArtist -> cmp ($artist), 0, "Name comparison with etree artist works");
$artist -> name("Erwaklap");
is($otherArtist -> cmp ($artist), 8, "Name comparison with a completely different name works");
$artist -> name("Ase me na zhsw th zwh mou");
is($otherArtist -> cmp ($artist), 20, "Name comparison with a more completely different name works");
$artist -> name("Mastorakhs");
is($otherArtist -> cmp ($artist), 5, "Name comparison with a kind of similar name works");

done_testing();

# :-)
