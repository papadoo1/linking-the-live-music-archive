#!/usr/bin/perl -w
# Similarity.pl --- 
# Author: Orestis Papadopoulo <papadoo1@papadoo-Dell-Netbook>
# Created: 17 Mar 2014
# Version: 0.01

use warnings;
use strict;

use lib "../lib";
use Similarity::MbSim;
use Artist::EtreeArtist;
use Artist::MbArtist;
use Test::More;

my $etreeArtist = new Artist::EtreeArtist(name => 'Cuuko',
                                          URI => 'http://etree.linkedmusic.org/artist/422ef220-4aac-012f-19e9-00254bd44c28');

my $mbArtist = new Artist::MbArtist(name => 'Cuuko',
                                          URI => 'http://www.last.fm/music/5');

my $mbsim = new Similarity::MbSim(   object =>  $mbArtist,
                                     subject => $etreeArtist,
                                     weight => 1
                                    );

is($mbsim -> weight, 1, "weight is correct");
ok($mbsim -> toFile eq 1, "export to file is working");

done_testing();

__END__

=head1 NAME

Similarity.pl - Describe the usage of script briefly

=head1 SYNOPSIS

Similarity.pl [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation for Similarity.pl, 

=head1 AUTHOR

Orestis Papadopoulo, E<lt>papadoo1@papadoo-Dell-NetbookE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2014 by Orestis Papadopoulo

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut
