#!/usr/bin/perl -w
# Using TDD for MusicBrainz Artist Object
use lib '../lib';
use strict;
use feature qw(say);
use Data::Dumper;
use Test::More;
use API::Etree;
use Artist::MbArtist;
use Artist;

my $opt = shift;

# Query for an artist that exists in the mb Set (not in the etree set)
my $mastodonQuery = API::MusicBrainz::searchArtist("Mastodon");
ok(scalar @{$mastodonQuery} gt 0, "Searching for an artist in MusicBrainz works!");
my $artist = @{$mastodonQuery}[0];

# check if the object is initialised correctly
is($artist->name(), 'Mastodon',
   "The first artist is the correct artist and name initialisation works!");
is($artist->URI(), 'http://musicbrainz.org/artist/bc5e2ad6-0a4a-4d90-b911-e9a7e6861727#_', "URI initialisation works");
is($artist->id(), 'bc5e2ad6-0a4a-4d90-b911-e9a7e6861727', 'id function works properly');

# recordings
ok(scalar keys $artist -> recordings() gt 0, "The recordings initialisation works");
is(scalar keys $artist -> recordings() ,111, "The right number of recordings was returned");
if ($opt eq "-v") {
    print Dumper $artist -> recordings();
}

my $feistQuery = API::MusicBrainz::searchArtist("Feist");
my $feist = @{$feistQuery}[0];
my $otherArtist = Artist -> new ( name => 'Mastodon', URI => 'fake.com' );

# Comparisons
is ($artist -> cmp ($otherArtist), 0, "comparison with object that doesn't implement RecordingComparable");
is ($artist -> cmp ($artist), 1, "comparison with object that implements RecordingComparable (itself actually)");
ok ($artist -> cmp ($feist) < 0.1, "comparison with other object that implements RecordingComparable");
is ($artist -> cmp($otherArtist), 0, "(name) comparison with other non-track comparable returns 0");

# test against etree Artists
my $etreeArtists = API::Etree::getArtists(5);
my $etreeArtist = @{$etreeArtists}[4];
# weight should be zero
is ($etreeArtist -> cmp ($artist), 0, "(track) comparison with etreeArtist");

done_testing();
