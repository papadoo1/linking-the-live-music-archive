#!/usr/bin/perl -w
use lib '../lib';
use strict;
use feature qw(say);
use Data::Dumper;
use Test::More;
use Matcher::MusicBrainzMatcher;
use Matcher::LastFMMatcher;

print "Do you want me to perform a long run test? for MusicBrainzMatcher.pm\n";
my $c = <STDIN>;
chomp $c;
if ($c eq 'y')
{
    my $mbmatcher = new Matcher::MusicBrainzMatcher(verbose => 0,
                                            fileExport => 1);
    is($mbmatcher -> setup(), 1, "Setup was run sucessfully");
    is($mbmatcher -> run(), 1, "Matcher was run sucessfully");
}

print "Do you want me to perform a long run test? for LastFMMatcher.pm\n";
$c = <STDIN>;
chomp $c;
if ($c eq 'y')
{
    my $lfmmatcher = new Matcher::LastFMMatcher(verbose => 0,
                                            fileExport => 1);
    is($lfmmatcher -> setup(), 1, "Setup was run sucessfully");
    is($lfmmatcher -> run(), 1, "Matcher was run sucessfully"); 
}

done_testing();





