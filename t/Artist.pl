#!/usr/bin/perl -w
# Trying to use TDD
use lib '../lib';
use strict;
use feature qw(say);
use Data::Dumper;
use Test::More;
use Artist::EtreeArtist;

my $artist = Artist->new(
                        name => 'Orestis',
                        URI => 'locknload.com',
                        );
my $otherArtist = Artist -> new ( name => 'Orestakis', URI => 'fake.com' );

# check if the object is initialised correctly
is($artist->name(), 'Orestis');
is($artist->URI(), 'locknload.com');

# check if the distance returned is correct
is($artist -> cmp($otherArtist), 2);

# for the same name it should return 0
$otherArtist -> name ('Orestis');
is($artist -> cmp($otherArtist), 0);

# this passes, but it may cause undefined behaviour in the cmp method
my $yetanotherartist = Artist -> new (URI => 'hehhe');
is($artist -> cmp($otherArtist), 0);

done_testing();







