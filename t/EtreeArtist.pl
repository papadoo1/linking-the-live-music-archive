#!/usr/bin/perl -w
# Trying to use TDD
use lib '../lib';
use strict;
use Class::ISA;
use feature qw(say);
use Data::Dumper;
use Test::More;
use Artist::EtreeArtist;
use Artist;

my $arg = shift;

my $artists = API::Etree::getArtists(30);
ok (scalar @{$artists} eq 30, 'Etree SPARQL endpoint is working');
is (@{$artists}[0] -> isa('Artist::EtreeArtist'), 1, 'getArtists methods returns EtreeArtist references');

my $artist = @{$artists}[20];
my $otherArtist = Artist -> new ( name => '37 Including You', URI => 'fake.com' );

# check if the object is initialised correctly
is($artist->name(), '37 Including You', "Name initialisation works");
is($artist->URI(), 'http://etree.linkedmusic.org/artist/42320a30-4aac-012f-19e9-00254bd44c28', "URI initialisation works");

ok(scalar keys $artist -> recordings() gt 0, "The recordings initialisation works");

# check if the distance returned is correct
is($artist -> cmp($otherArtist), 0, "(name) comparison with other non-track comparable works");

# for the same name it should return 0
$otherArtist -> name ('38 incldue');
is($artist -> cmp($otherArtist), 9, "(name) comparison for slightly different names works");

my $artisttocomparetracks = @{$artists}[18]; # get this etree artist
is($artist -> cmp($artisttocomparetracks), 0, "(track) comparison for different etree artists work");
if ($arg eq "-v") {
    print Dumper $artist -> recordings();
    print Dumper $artisttocomparetracks -> recordings();
}

done_testing();
